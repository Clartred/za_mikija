package rs.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import rs.model.Entry;
import rs.service.EntryService;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable {

    @FXML
    private TableView<Entry> tableView;
    @FXML
    private Button addButton;
    @FXML
    private Button editButton;
    @FXML
    private Button deleteButton;
    @FXML
    private TableColumn nameTC;
    @FXML
    private TableColumn lastNameTC;
    @FXML
    private TableColumn phoneNumberTC;
    @FXML
    private TableColumn addressTC;
    @FXML
    private TableColumn cityTC;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        setUpTableView();
        fillTableView();

        addButton.setOnAction(e -> {
            try {
                Stage stage = new Stage();
                Parent root = FXMLLoader.load(ClassLoader.getSystemResource("AddWindow.fxml"));
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.setMinWidth(280);
                stage.setMinHeight(350);
                stage.setResizable(false);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setTitle("!");
                stage.showAndWait();
                fillTableView();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        });

        deleteButton.setOnAction(e -> {
            if ( tableView.getSelectionModel().getSelectedItem() == null ) {
                return;
            }
            new EntryService().delete(tableView.getSelectionModel().getSelectedItem().getId());
            fillTableView();
        });

        editButton.setOnAction(e ->{
            if ( tableView.getSelectionModel().getSelectedItem() == null ) {
                return;
            }
            try {
                FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("EditWindow.fxml"));

                Stage stage = new Stage(StageStyle.DECORATED);
                stage.setScene(new Scene(loader.load()));
                EditWindowController editWindowController = loader.getController();
                editWindowController.initData(tableView.getSelectionModel().getSelectedItem());
                stage.setTitle("Edit Entry");
                stage.setResizable(false);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.showAndWait();
                fillTableView();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
    }

    private void setUpTableView() {
        tableView.getSelectionModel().setCellSelectionEnabled(true);
        nameTC.setCellValueFactory(new PropertyValueFactory<>("name"));
        lastNameTC.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        phoneNumberTC.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        addressTC.setCellValueFactory(new PropertyValueFactory<>("address"));
        cityTC.setCellValueFactory(new PropertyValueFactory<>("city"));
    }

    private void fillTableView() {
        ObservableList<Entry> entryList = FXCollections.observableArrayList(new EntryService().getAll());
        tableView.setItems(entryList);
    }

}
