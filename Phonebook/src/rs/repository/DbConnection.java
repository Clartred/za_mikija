package rs.repository;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DbConnection {

    private static Connection connection;
    private static String dbLocation = "jdbc:sqlite:phonebook.sqlite";

    public static Connection connectToDb() {

        if ( connection == null ) {
            try {
                connection = DriverManager.getConnection(dbLocation);
            } catch (SQLException ex) {
               ex.printStackTrace();
            }
        } else {
            return connection;
        }
        return connection;
    }

    public void checkIfDbExists() {
        String path = dbLocation;
        File f = new File(path);
        if ( !f.exists() ) {
            createNewDb();
        }
    }

    private void createNewDb() {

        String url = dbLocation;
        try {
            DriverManager.getConnection(url);
            createNewTables();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void createNewTables() {

        String napraviTabeluVrstaHrane = "CREATE TABLE IF NOT EXISTS entry(id INTEGER PRIMARY KEY, first_name varchar(255)," +
         "last_name varchar(255), phone_number varchar(255), address varchar(255), city varchar(255));";
        Connection conn = connectToDb();
        try {
            Statement s = conn.createStatement();
            s.execute(napraviTabeluVrstaHrane);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
