package rs.service;

import rs.model.Entry;
import rs.repository.EntryRepository;

import java.util.List;

public class EntryService {

    public void add(Entry e){
        new EntryRepository().add(e);
    }

    public Entry getById(int id){
        return null;
    }

    public void delete(int id){
        new EntryRepository().delete(id);
    }

    public List<Entry> getAll(){
        return new EntryRepository().getAll();
    }

    public void edit(Entry entry){
        new EntryRepository().editEntry(entry);
    }

}
