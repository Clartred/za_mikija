package rs.repository;

import rs.model.Entry;

import java.sql.*;
import java.util.ArrayList;

public class EntryRepository {

    public void add(Entry entry) {

        String unesi = "INSERT INTO entry(first_name, last_name, phone_number, address, city) VALUES (?, ?, ?, ?, ?)";
        Connection conn = DbConnection.connectToDb();
        try {
            PreparedStatement st = conn.prepareStatement(unesi);
            st.setString(1, entry.getName());
            st.setString(2, entry.getLastName());
            st.setString(3, entry.getPhoneNumber());
            st.setString(4, entry.getAddress());
            st.setString(5, entry.getCity());
            st.executeUpdate();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Entry> getAll() {
        String selectEntries = "SELECT * FROM entry";
        ArrayList<Entry> entryList = new ArrayList<>();

        try {
            Connection conn =  DbConnection.connectToDb();
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(selectEntries);
            while (rs.next()) {
                entryList.add( new Entry(
                        rs.getInt("id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getString("phone_number"),
                        rs.getString("address"),
                        rs.getString("city")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entryList;
    }

     public void delete(int id) {

         String deleteQuery = "DELETE FROM entry where id ='" + id + "'";
        try {
            PreparedStatement preparedStatement = DbConnection.connectToDb().prepareStatement(deleteQuery);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void editEntry(Entry entry) {

        String updateQuery = "UPDATE Entry set first_name = ?, last_name = ?, phone_number = ?, address = ?, city = ? WHERE id = '" + entry.getId() + "'";
        try {
            PreparedStatement st = DbConnection.connectToDb().prepareStatement(updateQuery);
            st.setString(1, entry.getName());
            st.setString(2, entry.getLastName());
            st.setString(3, entry.getPhoneNumber());
            st.setString(4, entry.getAddress());
            st.setString(5, entry.getCity());
            st.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
