package rs.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import rs.model.Entry;
import rs.service.EntryService;

import java.net.URL;
import java.util.ResourceBundle;

public class AddWindowController implements Initializable {

    @FXML
    private TextField nameTF;
    @FXML
    private TextField lastNameTf;
    @FXML
    private TextField phoneNumberTF;
    @FXML
    private TextField addressTF;
    @FXML
    private TextField cityTF;
    @FXML
    private Button acceptButton;
    @FXML
    private Button cancelButton;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        cancelButton.setOnAction(e -> {
          Stage stage = (Stage) cancelButton.getScene().getWindow();
          stage.close();
        });

        acceptButton.setOnAction(e -> {

            if(nameTF.getText() != "" && lastNameTf.getText() != "" && phoneNumberTF.getText() != "" && addressTF.getText() != "" && cityTF.getText() != ""){
                new EntryService().add(new Entry(nameTF.getText(), lastNameTf.getText(), phoneNumberTF.getText(), addressTF.getText(), cityTF.getText()));
            }
            Stage stage = (Stage) cancelButton.getScene().getWindow();
            stage.close();
        });


    }

}
