package rs.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import rs.model.Entry;
import rs.service.EntryService;

import java.net.URL;
import java.util.ResourceBundle;

public class EditWindowController implements Initializable {

    @FXML
    private TextField nameTF;
    @FXML
    private TextField lastNameTf;
    @FXML
    private TextField phoneNumberTF;
    @FXML
    private TextField addressTF;
    @FXML
    private TextField cityTF;
    @FXML
    private Button acceptButton;
    @FXML
    private Button cancelButton;
    private Entry entry;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void initData(Entry entry) {

        this.entry = entry;
        this.nameTF.setText(entry.getName());
        this.lastNameTf.setText(entry.getLastName());
        this.phoneNumberTF.setText(entry.getPhoneNumber());
        this.addressTF.setText(entry.getAddress());
        this.cityTF.setText(entry.getCity());


        acceptButton.setOnAction(e -> {
            if ( !nameTF.getText().equals("") && !lastNameTf.getText().equals("")
                    && !phoneNumberTF.getText().equals("") && !addressTF.getText().equals("")
                    && !cityTF.getText().equals("") ) {

                new EntryService().edit(
                        new Entry(
                                this.entry.getId(),
                                this.nameTF.getText(),
                                this.lastNameTf.getText(),
                                this.phoneNumberTF.getText(),
                                this.addressTF.getText(),
                                this.cityTF.getText()
                        )
                );

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("ALERT!");
                alert.setHeaderText(null);
                alert.setContentText("YOU CHANGED ENTRY!");
                alert.showAndWait();
                Stage stage = (Stage) acceptButton.getScene().getWindow();
                stage.close();
            }
        });
    }

}